import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { NisCheckComponent } from './nis-check/nis-check.component';
import { ExamplesComponent } from './examples/examples.component';
import { InstructionsComponent } from './instructions/instructions.component';

const routes: Routes = [
  {
    path: 'instructions',
    component: InstructionsComponent
  }, {
    path: 'examples',
    component: ExamplesComponent
  }, {
    path: '',
    component: NisCheckComponent,
    pathMatch: 'full'
  }, {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
