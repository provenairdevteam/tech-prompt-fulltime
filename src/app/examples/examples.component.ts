import { Component, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { FormGroup, Validators, FormBuilder, FormGroupDirective } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'pat-examples',
  templateUrl: 'examples.component.html'
})

export class ExamplesComponent implements OnInit {
  buttonClickCount = 0;
  addUserForm: FormGroup;

  dates: {
    from: Date,
    to: Date,
    max: Date
  };

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.dates = {
      from: new Date(),
      to: new Date(),
      max: new Date()
    };

    this.addUserForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      role: ['', [Validators.required]]
  });
  }

  addUser($event: Event) {

    const actorKey = this.addUserForm.get('email').value;
    const role = this.addUserForm.get('role').value;
    console.log(actorKey, role);

    this.addUserForm.reset();
  }

  toDateChanged(event: MatDatepickerInputEvent<Date>) {
    if (this.dates.to < this.dates.from) {
      this.dates.from = this.dates.to;
    }
    console.log(this.dates);
  }

  onButtonClick() {
    this.buttonClickCount++;
  }
}
