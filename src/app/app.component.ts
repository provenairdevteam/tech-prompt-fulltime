import { Component } from '@angular/core';

@Component({
  selector: 'pat-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Tech Prompt';
}
