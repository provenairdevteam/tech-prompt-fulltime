import { NisCheck } from './nis-check';

describe('nis-check', function() {
  describe('process', function() {
    testProcess([
      {
        serials: ['A'],
        from: new Date('2012-01-01'),
        to: new Date('2012-02-01')
      }
    ], [
      {
        serial: 'A',
        from: new Date('2012-01-01'),
        to: new Date('2012-02-01'),
        continuous: true
      }
    ]);
    testProcess([
      {
        serials: ['B'],
        from: new Date('2012-01-01'),
        to: new Date('2012-02-01')
      }, {
        serials: ['B'],
        from: new Date('2013-01-01'),
        to: new Date('2013-02-01')
      }
    ], [
      {
        serial: 'B',
        from: new Date('2012-01-01'),
        to: new Date('2013-02-01'),
        continuous: false
      }
    ]);
    testProcess([
      {
        serials: ['A', 'B'],
        from: new Date('2012-01-01'),
        to: new Date('2012-02-01')
      }, {
        serials: ['B', 'C'],
        from: new Date('2013-01-01'),
        to: new Date('2013-02-01')
      }, {
        serials: ['B'],
        from: new Date('2012-02-01'),
        to: new Date('2013-01-01')
      }
    ], [
      {
        serial: 'A',
        from: new Date('2012-01-01'),
        to: new Date('2012-02-01'),
        continuous: true
      }, {
        serial: 'B',
        from: new Date('2012-01-01'),
        to: new Date('2013-02-01'),
        continuous: true
      }, {
        serial: 'C',
        from: new Date('2013-01-01'),
        to: new Date('2013-02-01'),
        continuous: true
      }
    ]);
  });
});

function testProcess(documents, expected) {
  let _documents = [],
  _expected = [];
  beforeEach(function() {
    _documents = [].concat(documents);
    _expected = [].concat(expected);
  });

  it('has the same number of parts', function() {
    const result = NisCheck.process(_documents);
    expect(result.length).toEqual(_expected.length);
  });

  it('identifies all the parts', function() {
    const result = NisCheck.process(_documents);
    _expected.forEach(e => {
      const found = result.find(p => p.serial == e.serial);
      expect(found).toBeTruthy();
    });
  });

  it('has valid to and from dates', function() {
    const result = NisCheck.process(_documents);
    _expected.forEach(e => {
      const found = result.find(p => p.serial == e.serial);
      expect(found.to).toEqual(e.to);
      expect(found.from).toEqual(e.from);
    });
  });

  it('has the correct `continuous` value', function() {
    const result = NisCheck.process(_documents);
    _expected.forEach(e => {
      const found = result.find(p => p.serial == e.serial);
      expect(found.continuous).toEqual(e.continuous);
    });
  });
}
