export class NisDocument {
  // The unique part identifiers
  serials: string[];

  // The starting date
  from: Date;

  // The ending date
  to: Date;

  constructor() {
    this.serials = [];
    this.from = new Date();
    this.to = new Date();
  }
}
