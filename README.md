# ProvenAir Tech Prompt

## One-Time Setup

- Ensure [nodejs](https://nodejs.org/en/) is installed
- Open a command prompt to this directory
- Run the command `npm install`

## Running the application
- Open a command prompt to this directory
- Run the command `npm start` and browse to http://localhost:4200/
- Further instructions are in the application

## Running the tests
- Open a command prompt to this directory
- Run the command `npm test` to launch in watch mode
